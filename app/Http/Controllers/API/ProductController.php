<?php

namespace App\Http\Controllers\API;

use App\Helpers\ResponseFormatter;
use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use DB;
class ProductController extends Controller
{
    public function all(Request $request){
        $id =   $request->input('id');
        $limit = $request->input('limit');
        $name = $request->input('name');
        $description = $request->input('description');
        $tags = $request->input('tags');
        $categories = $request->input('categories');
        $price_form = $request->input('price_form');
        $price_to = $request->input('price_to');

        if ($id) {
            $product = Product::with(['category', 'galleries'])->find($id);

            if ($product) {
                return ResponseFormatter::success(
                    $product,
                    'Data product berhasil diambil'
                );
            }else{
                return ResponseFormatter::error(
                    null,
                    'Data product tidak ada'
                );
            }
        }

        $product = Product::with(['category', 'galleries']);

        if ($name) {
            $product->where('name', 'like', '%' . $name . '%');
        }

        if ($description) {
            $product->where('description', 'like', '%' . $description . '%');
        }

        if ($tags) {
            $product->where('tags', 'like', '%' . $tags . '%');
        }

        if ($price_form) {
            $product->where('price', '>=', $price_form);
        }

        if ($price_to) {
            $product->where('price', '<=', $price_to);
        }

        if ($categories) {
            $product->where('price',  $categories);
        }

        return ResponseFormatter::success(
            $product->paginate($limit),
            'Data product berhasil diambil'
        );


    }


    public function updateUrlProduct(){
      $data = DB::table('product_galleries')->get();
      foreach ($data as $key => $value) {

        if ($value->id % 2 == 0){ //Kondisi
            DB::table('product_galleries')->where('id', $value->id)->update([
              'url' => 'public/gallery/sepatu.png',
            ]);
        }else {
            DB::table('product_galleries')->where('id', $value->id)->update([
              'url' => 'public/gallery/sepatu2.png',
            ]);
        }
      }
      return 'OK';
      
    }
}
