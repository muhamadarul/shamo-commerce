<?php

namespace App\Http\Controllers\API;

use Exception;
use App\Models\User;
use Illuminate\Http\Request;
use App\Helpers\ResponseFormatter;
use Laravel\Fortify\Rules\Password;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function register(Request $request){
      
        try {
            $this->validate($request,[
                'name' => ['required','string','max:255'],
                'username' => ['required','string','max:255','unique:users'],
                'email' => ['required','string','max:255','unique:users'],
                // 'phone' => ['nullable','string','max:255'],
                'password' => ['required','string', new Password],
            ]);

            User::create([
                'name' => $request->name,
                'username' => $request->username,
                'email' => $request->email,
                'phone' => $request->phone,
                'password' => Hash::make($request->password),
            ]);

            $user = User::where('email', $request->email)->first();
            $tokenResult = $user->createToken('authToken')->plainTextToken;

            return ResponseFormatter::success([
                    'access_token' => $tokenResult,
                    'token_type' => 'Bearer',
                    'user' => $user
                ], 
                'User Registered'
            );

        } catch (Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error
            ], 
            'Authentication Failed', 400
            );
        }
    }

    public function login(Request $request){
        try {
            $this->validate($request,[
                'email' => ['required','email','max:255'],
                'password' => ['required','string'],
            ]);

            $credentials = request(['email', 'password']);

            if (!Auth::attempt($credentials)) {
                return ResponseFormatter::error([
                    'message' => 'Unauthorized'
                ], 
                'Authentication Failed', 400
                );
            }

            $user = User::where('email', $request->email)->first();

            if(!Hash::check($request->password, $user->password, [])){
                throw new \Exception('Invalid Credentials');
            }

            $tokenResult = $user->createToken('authToken')->plainTextToken;
            return ResponseFormatter::success([
                    'access_token' => $tokenResult,
                    'token_type' => 'Bearer',
                    'user' => $user
                ], 
                'Authenticated'
             );
        } catch (\Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error
            ], 
            'Authentication Failed', 400
            );
        }
    }

    public function fetch(Request $request){
        return ResponseFormatter::success($request->user(), 
        'Data Profile User Berhasil Diambil'
     );
    }

    public function updateProfile(Request $request){
        try {
            $this->validate($request,[
                'email' => ['required','email','string','max:255'],
                'username' => ['required','string','max:255'],
                'name' => ['required','string','max:255'],
            ]);

            $check_user = User::where('id', Auth::user()->id)
            ->where(function($query) {
                $query->where('id', '<>', Auth::user()->id)
                        ->orWhere('username', '<>', Auth::user()->username);;
            })->get();
            // dd($check_user);
            if($check_user->isNotEmpty()){
                return ResponseFormatter::error([
                    'message' => 'Email or username already taken'
                ], 
                'Update Profile Failed', 400
                );
            }


            $data = $request->all();
            $user = Auth::user();
            $user->update($data);
            
            return ResponseFormatter::success($user, 'Update Profile Berhasil');

        } catch (\Exception $error) {
            return ResponseFormatter::error([
                'message' => 'Something went wrong',
                'error' => $error
            ], 
            'Update Profile Failed', 400
            );
        }
    }

    public function logout(Request $request){
        $token = $request->user()->currentAccessToken()->delete();
        return ResponseFormatter::success($token, 'Token Revoked');
    }
}
